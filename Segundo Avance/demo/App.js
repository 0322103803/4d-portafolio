import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View} from 'react-native';

import ImageViewer from './components/ImageViewer';
import Button from './components/Button';

const PlaceholderImage = require('./assets/images/Imagen2.jpg');

export default function App() {
  return (
    <View style={styles.container}>

      <View style={styles.imageContainer}>
        <ImageViewer PlaceholderSource={PlaceholderImage}/>
      </View>

      <view style={styles.footerContainer}>
        <Button label="Choose a Photo" />
        <Button label="Use this Photo" />
      </view>

      <StatusBar style="auto"/>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#17202a',
    alignItems: 'center',
    justifyContent: 'center',
  },
  imageContainter: {
    flex: 1,
    paddingTop: 58,
  },
  footerContainer: {
    flex: 1 / 3,
    alignItems: 'center',
  }
});
