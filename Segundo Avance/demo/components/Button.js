import { StyleSheet, Pressable, Text, View} from "react-native";

export default function Button ({label}) {
    return(
        <View style={styles.buttonContainer}> 
            <Pressable style={styles.button} onPress={() => alert("You Pressed a Button")}>
                <Text style={styles.buttonLabel}>{label}</Text>
            </Pressable>
        </View>
    );
}

const styles = StyleSheet.create({
        buttonContainer: {
            width: 320,
            height: 68,
            marginHorizontal: 20,
            alignItems: 'center',
            justifyContent: 'center',
            padding: 3,
        },
        button: {
            borderRadius: 10,
            width: '100%',
            heigth: '100%',
            lignItems: 'center',
            justifyContent: 'center',
            flexDirection: 'row',
        },
        buttonLabel:{
            color: '#fff',
            fontSize: 16,
        },
        buttonIcon: {
            paddingTop: 8,
    },
    });


